import { Injectable } from '@angular/core';
import {DirectLine} from 'botframework-directlinejs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DirectLineService {

  private directLine: DirectLine;


  constructor() {
    this.directLine = new DirectLine({
      secret: environment.directLineKey,
      webSocket: true
    });
  }

  sendMessageToBot(text: string) {
    return this.directLine
      .postActivity({
        from: {id: environment.userId, name: environment.userName},
        type: 'message',
        text: text
      });
  }

  sendEventToBot(name: string, value?: string) {
    return this.directLine
      .postActivity({
        from: {id: environment.userId, name: environment.userName},
        type: 'event',
        name: name,
        value: value || ''
      });
  }

  subscribeBot() {
    return this.directLine.activity$;
  }
}

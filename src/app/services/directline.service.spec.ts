import { TestBed } from '@angular/core/testing';

import { DirectLineService } from './directline.service';

describe('DirectlineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DirectLineService = TestBed.get(DirectLineService);
    expect(service).toBeTruthy();
  });
});

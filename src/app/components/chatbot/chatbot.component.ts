import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Message} from 'botframework-directlinejs';
import {DirectLineService} from '../../services/directline.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit, AfterViewChecked {
  @ViewChild('messagesContainer') private messagesContainer: ElementRef;

  messages: Message[];
  messageInput = '';

  constructor(private directLineService: DirectLineService,
              private router: Router,
              private route: ActivatedRoute) {
    this.messages = [];

    this.directLineService.subscribeBot().subscribe((message: Message) => {
      if (message.from.id !== environment.userId) {
        this.messages.push(message);
      }
    });

    this.route.params.subscribe(params => {
      const userId = params['id'] || '1';

      this.directLineService.sendEventToBot('UpdateUser', userId).subscribe(id => {
        this.directLineService.sendEventToBot('WelcomeDialog').subscribe(id2 => {});
      });
    });
  }

  ngOnInit() {
  }

  scrollToBottom(): void {
    try {
      this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  sendMessage() {

    this.messages.push({
      text: this.messageInput,
      from: {id: environment.userId, name: environment.userName},
      type: 'message'
    });

    this.directLineService.sendMessageToBot(this.messageInput).subscribe(id => {

    });

    this.messageInput = '';
  }

  sendMessageKey(event) {
    if (event.keyCode === 13) {
      this.sendMessage();
    }
  }

  isUser(message: Message): boolean {
    return message.from.id === environment.userId;
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }
}

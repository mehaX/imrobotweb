import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { ChatbotComponent } from './components/chatbot/chatbot.component';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ChatbotComponent
  },
  {
    path: '*',
    component: ChatbotComponent
  },
  {
    path: ':id',
    component: ChatbotComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ChatbotComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
